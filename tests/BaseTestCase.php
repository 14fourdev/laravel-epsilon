<?php namespace Tests;

use Orchestra\Testbench\TestCase;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BaseTestCase extends TestCase {


    public function __construct() {

    }

    public function test_path() {
        return __DIR__;
    }


    /**
    * Setup the test environment.
    */
    // public function setUp()
    // {
    //
    //     parent::setUp();
    //
    // }




    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
    }

    //
    protected function getPackageProviders($app)
    {
        return [
            'Epsilon\EpsilonServiceProvider',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Epsilon' => 'Epsilon\Epsilon'
        ];
    }

}
