<?php namespace Epsilon\Validators;

class EpsilonPhone {


    /**
     * Phone Number Value
     * @var string
     */
    private $value;


    /**
     * Phone Numbers Valid State
     * @var boolean
     */
    private $valid = true;


    /**
     * Invlaid Exchanges (First 3 digits)
     * @var array
     */
    private $invalidExchanges = [
        '911',
        '411',
        '311',
        '976',
        '900',
    ];


    /**
     * Invalid Full Numbers
     * @var array
     */
    private $invalidNumbers = [
        '0123456789',
        '1234567890',
    ];


    /**
     * Invalid Phone Number Endings (last 7 digits)
     * @var array
     */
    private $invalidEndings = [
        '5551212',
    ];


    /**
     * [validatePhone description]
     * @param string $attribute  [description]
     * @param mixed|string|array $value      [description]
     * @param array  $parameters [description]
     * @param Validator $validator  [description]
     * @return boolean [description]
     */
    public function validatePhone( $attribute, $value, array $parameters, $validator ) {

        $this->value = (string)$value;

        $this->allNumbers();

        $this->characterCount();

        $this->checkExchange();

        $this->checkFullNumber();

        $this->checkNumberEnding();

        $this->repeatingDigets();

        return $this->valid;

    }


    /**
     * Check if the phone number characters are numeric
     * @return void
     */
    private function allNumbers( ) {

        if ( !is_numeric( $this->value ) ) {
            $this->valid = false;
        }

    }


    /**
     * Make sure that the character count is exactly 10
     * @return void
     */
    private function characterCount( ) {

        if ( strlen( $this->value ) !== 10 ) {
            $this->valid = false;
        }

    }


    /**
     * Check if the exchane is one of the excluded values from $this->invalidExchanges
     * @return void
     */
    private function checkExchange( ) {

        $exchange = substr($this->value, 0, 3);

        if ( in_array($exchange, $this->invalidExchanges) ) {
            $this->valid = false;
        }

    }


    /**
     * Check if the number is part of $this->invalidNumbers
     * @return void
     */
    private function checkFullNumber( ) {

        if ( in_array($this->value, $this->invalidNumbers) ) {
            $this->valid = false;
        }

    }


    /**
     * Check if the last 7 digits are part of $this->invalidEndings
     * @return void
     */
    private function checkNumberEnding() {

        $ending = substr($this->value, -7);

        if ( in_array($ending, $this->invalidEndings) ) {
            $this->valid = false;
        }

    }

    /**
     * Check if all characters are the same.
     * @return void
     */
    private function repeatingDigets() {

        if ( preg_match('/^(.)\1*$/', $this->value) ) {
            $this->valid = false;
        }

    }

}
