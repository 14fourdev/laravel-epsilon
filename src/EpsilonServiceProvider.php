<?php
namespace Epsilon;

use Illuminate\Support\ServiceProvider;

class EpsilonServiceProvider extends ServiceProvider {


    /**
     * Defer the loading of this service provider until it's called
     */
    // protected $defer = true;

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {

        $this->app->bind('Epsilon', function ($app) {
            return new Epsilon();
        });

    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {

        $this->publishes([
            __DIR__ . '/config/epsilon.php' => config_path('epsilon.php'),
        ], 'config');

        $this->app['validator']->extend('epsilonemail', 'Epsilon\Validators\EpsilonEmail@validateEmail');
        $this->app['validator']->extend('epsilonphone', 'Epsilon\Validators\EpsilonPhone@validatePhone');

    }

}
