<?php namespace Epsilon\Validators;
/**
 * A Validator for Epsilon Emails
 */


/**
 * Laravel Epsilon Email Validator
 *
 * @author Josh Hagel <josh@14four.com>
 *
 * @since v1.0.0
 */
class EpsilonEmail {


    /**
     * List of Email addressee that aren't valid in Eplison
     * @var array
     */
    private $invalidEmails = [
        'admin',
        'autoresponder',
        'abuse',
        'administrator',
        'alerts',
        'blank',
        'bulkmail',
        'contact',
        'devnull',
        'domain',
        'domreg',
        'domtech',
        'email',
        'ftp',
        'help',
        'hostmaster',
        'hr','info',
        'it',
        'jobs',
        'mailer-daemon',
        'marketing',
        'na',
        'news',
        'noc',
        'noemail',
        'none',
        'notgiven',
        'postmaster',
        'privacy',
        'remarks',
        'root',
        'route',
        'sales',
        'security',
        'spam',
        'support',
        'techsupport',
        'test',
        'unknown',
        'usenet',
        'uucp',
        'webmaster',
        'webteam',
        'www',
    ];


    /**
     * Validate that the input eamil is a valid email epsilon address
     *
     * @api
     *
     * @author Josh Hagel <josh@14four.com>
     *
     * @since v1.0.0
     *
     * @param string $attribute  [description]
     * @param mixed $value      [description]
     * @param array  $parameters [description]
     * @param Validator $validator  [description]
     * @return boolean [description]
     */
    public function validateEmail( $attribute, $value, array $parameters, $validator ) {

        if ( !$this->isEmail($value) || $this->checkInvalidEmails($value) ) {
            return false;
        }

        return true;

    }


    /**
     * Validate that the input value is a valid email address
     *
     * @author Josh Hagel <josh@14four.com>
     *
     * @since v1.0.0
     *
     * @param string $email Verify that the input is a valid eamil address
     * @return boolean
     */
    private function isEmail( $email ) {

        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;

    }


    /**
     * Validate that the input value is not included in $this->invalidEmails
     *
     * @author Josh Hagel <josh@14four.com>
     *
     * @since v1.0.0
     *
     * @param string $email Verify that the input is a valid eamil address
     * @return boolean
     */
    private function checkInvalidEmails( $email ) {

        $addressee = explode( '@', $email )[0];

        return in_array( $addressee, $this->invalidEmails );

    }


}
