<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Campaign ID
    |--------------------------------------------------------------------------
    |
    | Set client ID for the campaigns Young America Account. Setup override
    | in your .env file using YOUNGAMERICA_CLIENT
    |
    */
    'campaignId' => env('EPSILON_CAMPAIGNID', null),

    /*
    |--------------------------------------------------------------------------
    | Client ID
    |--------------------------------------------------------------------------
    |
    | Set client ID for the campaigns Young America Account. Setup override
    | in your .env file using YOUNGAMERICA_CLIENT
    |
    */
    'client' => env('EPSILON_CLIENT', null),


    /*
    |--------------------------------------------------------------------------
    | Secret
    |--------------------------------------------------------------------------
    |
    | Set the Vendor ID for your Young America Project. Setup override in your
    | .env file using EPSILON_SECRET
    |
    */
    'secret' => env('EPSILON_SECRET', null),

    /*
    |--------------------------------------------------------------------------
    | ENDPOINT
    |--------------------------------------------------------------------------
    |
    | Set the endpoint that the Epsilon API is going to hit. Setup
    | Override in your .env file. Default is production, for
    | development set this endpoint to
    |
    */
    'url' => env('EPSILON_URL', 'https://wsc-uat.peps.epsilon.com/CPGWebServices'),


];
