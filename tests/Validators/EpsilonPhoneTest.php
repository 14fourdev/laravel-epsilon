<?php namespace Tests\Validators;

use Tests\BaseTestCase;
use Artisan;
use Validator;
use Epsilon;

class EpsilonPhoneTest extends BaseTestCase {

    /**
     * Invlaid Exchanges (First 3 digits)
     * @var array
     */
    private $invalidExchanges = [
        '911',
        '411',
        '311',
        '976',
        '900',
    ];


    /**
     * Invalid Full Numbers
     * @var array
     */
    private $invalidNumbers = [
        '0123456789',
        '1234567890',
    ];


    /**
     * Invalid Phone Number Endings (last 7 digits)
     * @var array
     */
    private $invalidEndings = [
        '5551212',
    ];

    public function testValidatePhone()
    {

        $validation = [
            'phone' => 'epsilonphone',
        ];

        // Valid if empty (required used for validation of empty)
        $this->assertFalse( Validator::make( ['phone' => ''], $validation )->fails() );

        // Not Phone
        $this->assertTrue( Validator::make( ['phone' => 'aaabbbcccc'], $validation )->fails() );
        $this->assertTrue( Validator::make( ['phone' => 509448407], $validation )->fails() );
        $this->assertTrue( Validator::make( ['phone' => '509448407'], $validation )->fails() );
        $this->assertTrue( Validator::make( ['phone' => 50944840701], $validation )->fails() );
        $this->assertTrue( Validator::make( ['phone' => '50944840701'], $validation )->fails() );
        $this->assertTrue( Validator::make( ['phone' => '509448407d'], $validation )->fails() );

        // Invalid extentions
        foreach ( $this->invalidExchanges as $extention ) {
            $this->assertTrue( Validator::make( ['phone' => $extention . '4484070'], $validation )->fails() );
        }

        // Invalid Numbers
        foreach ( $this->invalidNumbers as $numbers ) {
            $this->assertTrue( Validator::make( ['phone' => $numbers], $validation )->fails() );
        }

        // Invalid Endings
        foreach ( $this->invalidEndings as $ending ) {
            $this->assertTrue( Validator::make( ['phone' => '509'. $ending], $validation )->fails() );
        }

        // Valid Phone Numbers
        $this->assertFalse( Validator::make( ['phone' => '5094484070'], $validation )->fails() );
        $this->assertFalse( Validator::make( ['phone' => 5094484070], $validation )->fails() );

    }

}
