<?php namespace Epsilon;

use App;
use Session;
use Config;
use Log;
use GuzzleHttp\Client;
use Validator;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;

/**
 * Interact with Epsilon API. Add, Update, Verify, and Login Users. Also allows for submision of survey requests, and validats the tokens for the server.
 *
 * @todo Inline Documenation
 * @todo Write Tests
 *
 * @author Josh Hagel <josh@14four.com>
 *
 * @since v1.0.0
 */
class Epsilon {



    public $failOnError = true;


    private $vendor_token;


    private $user_token;


    public $url;


    public $errors = [];


    public $validation = [
        'ClientID' => 'required',
        'FirstName' => 'max:255',
        'LastName' => 'max:255',
        'Emails.*.EmailAddr' => 'required|epsilonemail',
        'Phones.*.PhoneNumber' => 'epsilonphone',
    ];


    private $data = [
        'ClientID' => '',
        'TransactionID' => '',
        'FirstName' => '',
        'MiddleInit' => '',
        'LastName' => '',
        'Gender' => '',
        'NameSuffix' => '',
        'BirthDate' => '',
        'LanguageCode' => '',
        'SalutationID' => '',
        'SourceCode' => '',
        'AccountVerify' => '',
        'AccountVerifyDate' => null,
        'CountryCode' => '',
        'UserID' => '',
        'ProfilePassword' => '',
        'ProfilePasswordSalt' => '',
        'EncryptionType' => 0,
        'Address' => [
            'AddressID' => '',
            'AddressLine1' => '',
            'AddressLine2' => '',
            'City' => '',
            'State' => '',
            'PostalCode' => '',
            'Country' => '',
            'ChannelCode' => 'AD',
            'ChannelLocationID' => 'P',
            'DeliveryStatus' => 'A',
            'Status' => 'A',
            'IsPreferred' => 'Y',
        ],
        'Emails' => [],
        'SocialProfiles' => [],
    ];





    public function __construct() {

        if ( !Config::get('epsilon.client') ) {
            App::abort(500, 'Client ID not set in /config/epsilon.php');
        }

        if ( !Config::get('epsilon.secret') ) {
            App::abort(500, 'Secret not set in /config/epsilon.php');
        }

        if ( !Config::get('epsilon.url') ) {
            App::abort(500, 'Secret not set in /config/epsilon.php');
        } else {
            $this->url = Config::get('epsilon.url');
        }

        $this->initializeData();

        $this->verifyToken();

    }


    private function initializeData() {

        $this->ClientID = Config::get('epsilon.client');

    }


    ///////////////////////
    // Getters & Setters //
    ///////////////////////


    public function __get( $name ) {

        if ( $name === 'Email' && count($this->data['Emails']) > 0 ) {
            return $this->data['Emails'][0]['EmailAddr'];
        }

        if ( array_key_exists($name, $this->data) ) {
            return $this->data[$name];
        }

        return null;
    }


    public function __set( $name, $value ) {

        if ( $name === 'Email' ) {

            $this->updateEmail( $value );

        } else if ( $name === 'address' ) {

            $this->updateAddress($value);

        } else {

            $this->data[$name] = $value;

        }

    }


    ////////////
    // Tokens //
    ////////////

    /**
     * Verify That the vendor token has been set & request token if it hasn't
     * @return void
     */
    public function verifyToken() {

        $token = Session::get('epsilon.vendor.token');

        if ( $token ) {

            if ( date('U') < Session::get('epsilon.vendor.token.expires') ) {

                $this->vendor_token = $token;
                $this->TransactionID = Session::get('epsilon.transactionId');

            } else {

                $this->authorizeVendor();

            }

        } else {

            $this->authorizeVendor();

        }

        return true;

    }


    private function authorizeVendor() {

        // Setup Guzzle Client
        $client = new Client();

        $requestParams = '?grant_type=vendor';
        $requestParams .= '&client_id=' . Config::get('epsilon.client');
        $requestParams .= '&client_secret=' . Config::get('epsilon.secret');

        // Make Request
        $response = $client->request('POST', $this->url . '/OAuth2/AuthenticateVendor/Authorize' . $requestParams, []);

        $body = json_decode($response->getBody());

        $error = $this->checkForErrors( $body );

        if ( !$error ) {

            $this->vendor_token = $body->AccessToken;
            Session::set('epsilon.vendor.token', $this->vendor_token);
            Session::set('epsilon.vendor.token.exires', date('U') + $body->Expires);

            // Get A Transaction ID
            $this->getTransactionId();

        } else {

            App::abort(500, 'Epsilon - Could not connect to for vendor token');

        }

        return true;

    }


    private function getTransactionId() {

        // Setup Guzzle Client
        $client = new Client();

        // Make Request
        $response = $client->request('POST', $this->url . '/Transaction/GetTransactionID/data', []);

        $body = json_decode($response->getBody());

        $error = $this->checkForErrors( $body );

        if ( !$error && !empty($body) ) {

            Session::set('epsilon.transactionId', $body);

        } else {

            App::abort(500, 'Epsilon - Could not connect get a Transaction Id');

        }

        return true;

    }



    /////////////
    // Emails  //
    /////////////

    /**
     * Add an Email Address to the Emails Array in data.
     * @param String $email string of an email address.
     */
    public function addEmail( $email ) {

        $next = count($this->data['Emails']);

        return $this->updateEmail($email, $next);

    }


    /**
     * Update or add an email address at an index in the data['Emails'] array
     * @param string  $email Email Address that needs to be added to the email;
     * @param integer $index [description]
     * @return object  $this
     */
    public function updateEmail( $email, $index = 0) {

        $this->data['Emails'][0] = [
            'EmailID' => '',
            'EmailAddr' => $email,
            'ChannelCode' => 'EM',
            'ChannelLocationID' => 'H',
            'DeliveryStatus' => 'A',
            'Status' => 'A',
            'IsPreferred' => 'Y',
        ];

        return $this;

    }


    /////////////////////
    // Social Profiles //
    /////////////////////

    public function addSocialProfile( $userID, $provider, $SocialAccessToken = '' ) {

        $this->data['SocialProfiles'][] = [
            'SocialProfileID' => '',
            'SocialProvider' => $provider,
            'SocialUID' => $userID,
            'SocialAccessToken' => $SocialAccessToken,
            'Status' => 'A',
        ];

        return $this;

    }



    /////////////
    // Adress  //
    /////////////

    public function updateAddress( $values ) {

        foreach( $values as $key => $val ) {
            $this->data['Address'][$key] = $val;
        }

        return $this;

    }



    //////////
    // User //
    //////////

    public function authenticateUser() {

        $body = $this->request( '/OAuth2/AuthenticateUser/Authorize' );

    }


    public function authenticateSocial() {

        // $body = $this->request( '/OAuth2/AuthenticateSocialUser/Authorize' );

    }


    public function addUser() {

        if ( !$this->validate() ) {
            App::abort(500, 'Epsilon Add: Data Failed Validation');
        }

        $result = $this->request( '/Profile/AddProfile/data', $this->data, 'vendor' );

        if ( $result->NewProfileID ) {
            $this->data['ProfileID'] = $result->NewProfileID;
        }

        return $result;

    }

    public function updateUser() {

        $body = $this->request( '/Profile/UpdateProfile/data' );

    }


    public function verifyUser() {

        $body = $this->request( '/Profile/VerifyUser/data' );

    }



    public function userProfile( ) {

        if ( !$this->validate() ) {
            App::abort(500, 'Epsilon Add: Data Failed Validation');
        }

        $result = $this->request( '/Profile/GetProfileDetails/data', $this->data, 'vendor' );

        if ( $result->ProfileDetails->ProfileID ) {
            $this->data['ProfileID'] = $result->ProfileDetails->ProfileID;
        }

        return $result;

    }


    ////////////
    // Survey //
    ////////////

    public function surveyResponse( array $answers ) {

        $validation = [
            'ClientID' => 'required',
            'ProfileID' => 'required|string',
            'ProgramCode' => 'string',
            'TransactionID' => 'string',
            'ExternalInfo' => 'string',
            'SurveyProfileResponses.*.AnswerID' => 'string',
            'SurveyProfileResponses.*.SurveyQuestionAnswerID' => 'required|string',
            'SurveyProfileResponses.*.SurveyTextResponse' => 'string|max:100',
            'SurveyProfileResponses.*.DeleteResponseFlag' => 'string|max:1',
        ];

        $data = [
            'ClientID' => $this->ClientID,
            'ProfileID' => $this->ProfileID,
            'ProgramCode' => '',
            'TransactionID' => $this->TransactionID,
            'ExternalInfo' => '',
            'SurveyProfileResponses' => []
        ];

        foreach( $answers as $key => $val ) {
            $data['SurveyProfileResponses'][] = [
                'AnswerID' => '',
                'SurveyQuestionAnswerID' => (string)$key,
                'SurveyTextResponse' => $val,
                'DeleteResponseFlag' => 'N'
            ];
        }

        if ( !$this->validate($data, $validation) ) {
            App::abort(500, 'Epsilon Survey Response: Data Failed Validation');
        }

        $body = $this->request( '/Survey/AddSurveyProfileResponses/data', $data, 'vendor' );

        return $body;

    }


    ///////////////
    // Promotion //
    ///////////////

    /**
     * Add a Promotion Response to Epsilon. AddPromotionResponses are used to track programs/promotions/sweepstakes/actions/product usage, etc. in the Epsilon database, or to force an action like sending a Forgot Password email. 1 or many Campaign Control Ids (CCID) will be provided by Epsilon for each integration.
     * @return array An array of promotional responses or a single promotional responce
     */
    public function promotionResponse( array $promotions ) {

        $validation = [
            'ClientID' => 'required',
            'ProfileID' => 'required|string',
            'ProgramCode' => 'string',
            'TransactionID' => 'string',
            'ExternalInfo' => 'string',
            'PromotionResponses.*.RecordDate' => 'required|string',
            'PromotionResponses.*.CampaignControlID' => 'required',
        ];

        if ( $promotions['RecordDate'] ) {
            $responses = [$promotions];
        } else  {
            $responses = $promotions;
        }

        foreach( $responses as $key => $val ) {
            $responses[$key]['RecordDate'] = $this->formatJSONDate( $val['RecordDate'] );
        }

        $data = [
            'ClientID' => $this->ClientID,
            'TransactionID' => $this->TransactionID,
            'ProfileID' => $this->ProfileID,
            'EmailAddress' => $this->Email,
            'PromotionResponses' => $responses,
            'ExternalInfo' => '',
            'CultureInfo' => '',
        ];

        if ( !$this->validate($data, $validation) ) {
            App::abort(500, 'Epsilon Survey Response: Data Failed Validation');
        }

        $body = $this->request( '/Promotion/AddPromotionResponses/data', $data, 'vendor' );

        return $body;

    }


    ///////////////
    // API Calls //
    ///////////////

    /**
     * Formats Date Time & strings to JSON date
     * @param string|Carbon\Carbon|DateTime $date Object or string to be converted to timestamp.
     * @return string The JSON formated String.
     */
    public function formatJSONDate( $date ) {

        if ( $date instanceof DateTime ) {

            $timestamp = $date->getTimestamp() * 1000;

        } else if ( $date instanceof Carbon ) {

            $timestamp = $date->timestamp * 1000;

        } else {

            $timestamp = round(time() * 1000);

        }

        return "/Date($timestamp)/";
    }


    /**
     * Run Validation on the Data that will be provided to the API
     * @param boolean|array $data       An array of data that will be validated, if false will be $this->data
     * @param boolean|array $validation An array of validation rules for the data, if false will be $this->validation
     * @return boolean  True if the $data passes validation
     */
    public function validate( $data = false, $validation = false ) {

        if ( !$data ) {
            $data = $this->data;
        }

        if ( !$validation ) {
            $validation = $this->validation;
        }

        $validator = Validator::make( $data, $validation );

        if ($validator->fails()) {

            $this->errors = $validator->failed();

            Log::error("Epsilon Survey Response: Data Failed Validation: " . print_r($this->errors, true));

            return false;

        }

        return true;

    }


    /**
     * Makes a request to the epsilon API
     * @param string  $endpoint      This is the endpoint without the
     * @param array  $data          An array of the data that will be json_encoded and sent to the API.
     * @param boolean|string $include_token If the token is suppose to be sent with the request. Options: true = 'vendor', 'vendor', or 'user'.
     * @return string|object  The response from the API.
     */
    private function request( $endpoint, $data, $include_token = false ) {

        // Set the URL
        $url = Config::get('epsilon.url') . $endpoint;

        // Define Custom Headers
        $headers = [];

        if ( $include_token == 'user' ) {
            $headers['Authorization'] = 'USER ' . $this->user_token;
        } else if ( $include_token ) {
            $headers['Authorization'] = 'VENDOR ' . $this->vendor_token;
        }

        $client = new Client();

        try {
            $response = $client->request('POST', $url, [
                'headers' => $headers,
                'json' => $data
            ]);
        } catch (ClientException $e) {
            Log::error( $e->getResponse()->getBody()->getContents() );
            App::abort(500, 'Epsilon Request: Failed');
        }

        $body = json_decode($response->getBody());

        $error = $this->checkForErrors( $body );

        if ( $error && $failOnError ) {
            App::abort(500, 'Epsilon Request: Failed');
        }

        return $body;

    }


    /**
     * Check the API Response for Errors.
     * @param array $response The response from the API
     * @return boolean False if not errors were found. True if errors were found.
     */
    public function checkForErrors( $response ) {

        Log::info(print_r($response, true));

        if (( empty($response->Success) && !empty($response->Type) && $response->Type == 'ERROR' ) || ( !empty($response->Success) && !$response->Success ) || empty($response) || !empty($response->ErrorDesc) ) {

            return true;

        }

        return false;

    }
}
