<?php namespace Tests;

use Tests\BaseTestCase;
use Artisan;
use Validator;
use Epsilon;

class EpsilonTest extends BaseTestCase {

    public function testConstruction() {

        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\HttpException');

        $this->expectException( new Epsilon );

    }

}
