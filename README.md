# Laravel Young America

Integrate with Epsilon.

[TOC]

---
## Installation

### 1) Include Package With Composer

| Laravel Version | Package Version |
|-----------------|-----------------|
| 5.2             | 1.0.*           |

__Example__
```json
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/14fourdev/laravel-epsilon.git"
    }
  ],
  "require": {
    "14four/laravel-epsilon": "~1.0"
  }
}
```

__Run Compose__
```
php composer update
```

### 2) Add Service Provider and Facade

__{laravelroot}/config/app.php__

This will allow the config for Young America to be published and provide a `Epsilon` so you can access the class without requiring it.

```php
<?php

return [
    ...
    'providers' => [
        ...
        Epsilon\EpsilonServiceProvider::class,
        ...
    ],
    ...
    'aliases' => [
        ...
        'Epsilon' => Epsilon\YoungAmerica::class,
        ...
    ],
];
```

### 3) Publish Config File

```
php artisan vendor:publish --provider="Epsilon\EpsilonServiceProvider"
```

### 4) Add Validation Error message to Language Files

```
"epsilonemail" => "The :attribute field contains an invalid email.",

"epsilonphone" => "The :attribute field contains an invalid number.",
```

---
## Usage


### Create a New Consumer

```php
<?php namespace App\Acme;

use Epsilon;
...
    public function yourController() {

        $epsilon = new Epsilon;

        $epsilon->FirstName = 'Albert';
        $epsilon->LastName = '14Four';
        $epsilon->Email = 'albert@14four.com';

        $response = $epsilon->addUser();

        $user->epsilonId = $epsilon->ProfileID;

    }

```
**Example Response**
```json
{
  "MTServerName": null,
  "Type": "PROCESSED",
  "NewProfileID": "245396",
  "Additional": "AddedProfile",
  "ExternalInfo": null
}
```


### Survey response

> SurveyProfileResponses is typically used for Survey Question Answer Pairs and Subscriptions. Epsilon will provide vendors with the SQAID (SurveyQuestionAnswerId) needed for each implementation. For Subscriptions, 2 SQAIDs will be provided – one for Opt Ind and one for Opt Out.

There will only be 2 SQAIDs if there is an option for opting out. If there isn't an option to opt out there will only be 1 SQAID provided

**Note:** Multiple survey responses for a given Profile Id can be passed with a single transaction.

```php
<?php namespace App\Acme;

use Epsilon;
...
    public function yourController() {

        $survey = [
            '200864' => ''
        ];

        $epsilon = new Epsilon;

        $epsilon->ProfileID = '123456';

        $epsilon->serveryResponce( $survey );

    }

```
**Example Response**
```json
{
  "MTServerName": null,
  "Type": "PROCESSED",
  "Additional": null,
  "ExternalInfo": null
}
```
