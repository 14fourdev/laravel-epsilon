# Installation

Installation will be handled through a private VCS in composer. You'll need to configure the VCS before your can import the files.


## 1) Include Package With Composer

| Laravel Version | Package Version |
|-----------------|-----------------|
| 5.2             | 1.0.*           |

__Example__
```json
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/14fourdev/laravel-epsilon.git"
    }
  ],
  "require": {
    "14four/laravel-epsilon": "~1.0"
  }
}
```

__Run Compose__
```
php composer update
```


## 2) Add Service Provider and Facade

__{laravelroot}/config/app.php__

This will allow the config for Young America to be published and provide a `Epsilon` so you can access the class without requiring it.

```php
<?php

return [
    ...
    'providers' => [
        ...
        Epsilon\EpsilonServiceProvider::class,
        ...
    ],
    ...
    'aliases' => [
        ...
        'Epsilon' => Epsilon\Epsilon::class,
        ...
    ],
];
```


## 3) Publish Config File

```
php artisan vendor:publish --provider="Epsilon\EpsilonServiceProvider"
```


## 4) Add Validation Error message to Language Files

```
"epsilonemail" => "The :attribute field contains an invalid email.",

"epsilonphone" => "The :attribute field contains an invalid number.",
```
