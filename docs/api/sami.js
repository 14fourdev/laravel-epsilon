
(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:Epsilon" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Epsilon.html">Epsilon</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Epsilon_Validators" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Epsilon/Validators.html">Validators</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Epsilon_Validators_EpsilonEmail" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Epsilon/Validators/EpsilonEmail.html">EpsilonEmail</a>                    </div>                </li>                            <li data-name="class:Epsilon_Validators_EpsilonPhone" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Epsilon/Validators/EpsilonPhone.html">EpsilonPhone</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:Epsilon_Epsilon" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="Epsilon/Epsilon.html">Epsilon</a>                    </div>                </li>                            <li data-name="class:Epsilon_EpsilonServiceProvider" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="Epsilon/EpsilonServiceProvider.html">EpsilonServiceProvider</a>                    </div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "Epsilon.html", "name": "Epsilon", "doc": "Namespace Epsilon"},{"type": "Namespace", "link": "Epsilon/Validators.html", "name": "Epsilon\\Validators", "doc": "Namespace Epsilon\\Validators"},
            
            {"type": "Class", "fromName": "Epsilon", "fromLink": "Epsilon.html", "link": "Epsilon/Epsilon.html", "name": "Epsilon\\Epsilon", "doc": "&quot;Interact with Epsilon API. Add, Update, Verify, and Login Users. Also allows for submision of survey requests, and validats the tokens for the server.&quot;"},
                                                        {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method___construct", "name": "Epsilon\\Epsilon::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method___get", "name": "Epsilon\\Epsilon::__get", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method___set", "name": "Epsilon\\Epsilon::__set", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_verifyToken", "name": "Epsilon\\Epsilon::verifyToken", "doc": "&quot;Verify That the vendor token has been set &amp;amp; request token if it hasn&#039;t&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_addEmail", "name": "Epsilon\\Epsilon::addEmail", "doc": "&quot;Add an Email Address to the Emails Array in data.&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_updateEmail", "name": "Epsilon\\Epsilon::updateEmail", "doc": "&quot;Update or add an email address at an index in the data[&#039;Emails&#039;] array&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_addSocialProfile", "name": "Epsilon\\Epsilon::addSocialProfile", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_updateAddress", "name": "Epsilon\\Epsilon::updateAddress", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_authenticateUser", "name": "Epsilon\\Epsilon::authenticateUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_authenticateSocial", "name": "Epsilon\\Epsilon::authenticateSocial", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_addUser", "name": "Epsilon\\Epsilon::addUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_updateUser", "name": "Epsilon\\Epsilon::updateUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_verifyUser", "name": "Epsilon\\Epsilon::verifyUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_userProfile", "name": "Epsilon\\Epsilon::userProfile", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_serveryResponce", "name": "Epsilon\\Epsilon::serveryResponce", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_promotionResponse", "name": "Epsilon\\Epsilon::promotionResponse", "doc": "&quot;Add a Promotion Response to Epsilon. AddPromotionResponses are used to track programs\/promotions\/sweepstakes\/actions\/product usage, etc. in the Epsilon database, or to force an action like sending a Forgot Password email. 1 or many Campaign Control Ids (CCID) will be provided by Epsilon for each integration.&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_formatJSONDate", "name": "Epsilon\\Epsilon::formatJSONDate", "doc": "&quot;Formats Date Time &amp;amp; strings to JSON date&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_validate", "name": "Epsilon\\Epsilon::validate", "doc": "&quot;Run Validation on the Data that will be provided to the API&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\Epsilon", "fromLink": "Epsilon/Epsilon.html", "link": "Epsilon/Epsilon.html#method_checkForErrors", "name": "Epsilon\\Epsilon::checkForErrors", "doc": "&quot;Check the API Response for Errors.&quot;"},
            
            {"type": "Class", "fromName": "Epsilon", "fromLink": "Epsilon.html", "link": "Epsilon/EpsilonServiceProvider.html", "name": "Epsilon\\EpsilonServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Epsilon\\EpsilonServiceProvider", "fromLink": "Epsilon/EpsilonServiceProvider.html", "link": "Epsilon/EpsilonServiceProvider.html#method_register", "name": "Epsilon\\EpsilonServiceProvider::register", "doc": "&quot;Register bindings in the container.&quot;"},
                    {"type": "Method", "fromName": "Epsilon\\EpsilonServiceProvider", "fromLink": "Epsilon/EpsilonServiceProvider.html", "link": "Epsilon/EpsilonServiceProvider.html#method_boot", "name": "Epsilon\\EpsilonServiceProvider::boot", "doc": "&quot;Perform post-registration booting of services.&quot;"},
            
            {"type": "Class", "fromName": "Epsilon\\Validators", "fromLink": "Epsilon/Validators.html", "link": "Epsilon/Validators/EpsilonEmail.html", "name": "Epsilon\\Validators\\EpsilonEmail", "doc": "&quot;Laravel Epsilon Email Validator&quot;"},
                                                        {"type": "Method", "fromName": "Epsilon\\Validators\\EpsilonEmail", "fromLink": "Epsilon/Validators/EpsilonEmail.html", "link": "Epsilon/Validators/EpsilonEmail.html#method_validateEmail", "name": "Epsilon\\Validators\\EpsilonEmail::validateEmail", "doc": "&quot;Validate that the input eamil is a valid email epsilon address&quot;"},
            
            {"type": "Class", "fromName": "Epsilon\\Validators", "fromLink": "Epsilon/Validators.html", "link": "Epsilon/Validators/EpsilonPhone.html", "name": "Epsilon\\Validators\\EpsilonPhone", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Epsilon\\Validators\\EpsilonPhone", "fromLink": "Epsilon/Validators/EpsilonPhone.html", "link": "Epsilon/Validators/EpsilonPhone.html#method_validatePhone", "name": "Epsilon\\Validators\\EpsilonPhone::validatePhone", "doc": "&quot;[validatePhone description]&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


