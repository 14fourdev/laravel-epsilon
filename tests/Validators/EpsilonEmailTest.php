<?php namespace Tests\Validators;

use Tests\BaseTestCase;
use Artisan;
use Validator;
use Epsilon;

class EpsilonEmailTest extends BaseTestCase {


    /**
     * List of Email addressee that aren't valid in Eplison
     * @var array
     */
    private $invalidEmails = [
        'admin',
        'autoresponder',
        'abuse',
        'administrator',
        'alerts',
        'blank',
        'bulkmail',
        'contact',
        'devnull',
        'domain',
        'domreg',
        'domtech',
        'email',
        'ftp',
        'help',
        'hostmaster',
        'hr','info',
        'it',
        'jobs',
        'mailer-daemon',
        'marketing',
        'na',
        'news',
        'noc',
        'noemail',
        'none',
        'notgiven',
        'postmaster',
        'privacy',
        'remarks',
        'root',
        'route',
        'sales',
        'security',
        'spam',
        'support',
        'techsupport',
        'test',
        'unknown',
        'usenet',
        'uucp',
        'webmaster',
        'webteam',
        'www',
    ];


    public function testValidateEmail()
    {

        $validation = [
            'email' => 'epsilonemail',
        ];

        // Valid if empty (required used for validation of empty)
        $this->assertFalse( Validator::make( ['email' => ''], $validation )->fails() );

        // Not Emails
        $this->assertTrue( Validator::make( ['email' => 1], $validation )->fails() );
        $this->assertTrue( Validator::make( ['email' => 'albert'], $validation )->fails() );
        $this->assertTrue( Validator::make( ['email' => 'albert@'], $validation )->fails() );
        $this->assertTrue( Validator::make( ['email' => 'albert@14four'], $validation )->fails() );
        $this->assertTrue( Validator::make( ['email' => 'albert@14four.'], $validation )->fails() );

        foreach ( $this->invalidEmails as $email ) {

            $this->assertTrue( Validator::make( ['email' => $email . '@14four'], $validation )->fails() );

        }


        $this->assertFalse( Validator::make( ['email' => 'albert@14four.com'], $validation )->fails() );

    }

}
